package com.ufsj.ocs.projetoocs.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.Coordenadas;
import com.ufsj.ocs.projetoocs.model.Veiculo;

public class CadastroInformacoesPostosActivity extends AppCompatActivity {
    private EditText nome;
    private EditText gasolinaComum;
    private EditText gasolinaAditivada;
    private EditText etanol;
    private EditText diesel;
    private Button cadastrar;
    private String idPosto;
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceAux;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_informacoes_postos);
        Preferencias preferencias = new Preferencias(CadastroInformacoesPostosActivity.this);
        nome = (EditText) findViewById(R.id.informacoesNomeCadastroPosto);
        gasolinaComum = (EditText) findViewById(R.id.informacoesPrecoGasolinaComumPosto);
        gasolinaAditivada = (EditText) findViewById(R.id.informacoesPrecoGasolinaAditivadaPosto);
        diesel = (EditText) findViewById(R.id.informacoesPrecoDieselPosto);
        etanol= (EditText) findViewById(R.id.informacoesPrecoEtanolPosto);
        cadastrar = (Button) findViewById(R.id.cadastrarPostoFinalButton);

        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String gasCom,gasAdd,dsel,etan;
                gasCom = gasolinaComum.getText().toString();
                gasAdd = gasolinaAditivada.getText().toString();
                dsel = diesel.getText().toString();
                etan = etanol.getText().toString();
                if(!nome.getText().toString().isEmpty()){
                    if(!gasCom.isEmpty() || !gasAdd.isEmpty() || !dsel.isEmpty() || !etan.isEmpty()){
                        if(!gasCom.isEmpty() && !gasAdd.isEmpty() && !dsel.isEmpty() && !etan.isEmpty()){
                            cadastrarPosto();
                        }else{
                            if((!gasAdd.isEmpty() && Double.parseDouble(gasAdd)<0) || (!gasCom.isEmpty() && Double.parseDouble(gasCom)<0) ||
                                    (!dsel.isEmpty() && Double.parseDouble(dsel)<0) || (!etan.isEmpty() && Double.parseDouble(etan)<0)){

                                Toast.makeText(CadastroInformacoesPostosActivity.this, "Erro: Preço negativo inválido.",Toast.LENGTH_LONG).show();
                            }else{
                                criarAlertaPrecos();
                            }
                        }

                    }else{
                        Toast.makeText(CadastroInformacoesPostosActivity.this, "Erro: Todos os preços de combustíveis vazios. Favor inserir pelo menos um preço.",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(CadastroInformacoesPostosActivity.this, "Erro: Nome do posto não inserido.",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void criarAlertaPrecos(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(CadastroInformacoesPostosActivity.this);
        alertDialog.setTitle("Preços vazios");
        alertDialog.setIcon(R.drawable.ic_local_gas_station);
        alertDialog.setMessage("Existem preços de combustíveis vazios ou nulos. Eles serão registrados com valor 'R$0,00'. Deseja continuar?");

        alertDialog.setPositiveButton("Cadastrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                cadastrarPosto();
            }
        });

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertDialog.create();
        alertDialog.show();
    }

    private void cadastrarPosto(){

        final Preferencias preferencias = new Preferencias(CadastroInformacoesPostosActivity.this);
        final String idUsuario = preferencias.getId();
        idPosto = Coordenadas.converterIdCoordenadas(Float.parseFloat(preferencias.getPostoPadrao().getCoordenadas().getLatitude()),
                Float.parseFloat(preferencias.getPostoPadrao().getCoordenadas().getLongitude()));
        double precoGasCom, precoGasAdd, precoDsel, precoEtan;

        if(gasolinaComum.getText().toString().isEmpty()){
            precoGasCom = 0.0f;
        }else{
            precoGasCom = Double.parseDouble(gasolinaComum.getText().toString());
        }
        if(gasolinaAditivada.getText().toString().isEmpty()){
            precoGasAdd = 0.0f;
        }else{
            precoGasAdd = Double.parseDouble(gasolinaAditivada.getText().toString());
        }
        if(diesel.getText().toString().isEmpty()){
            precoDsel = 0.0f;
        }else{
            precoDsel = Double.parseDouble(diesel.getText().toString());
        }
        if(etanol.getText().toString().isEmpty()){
            precoEtan = 0.0f;
        }else{
            precoEtan = Double.parseDouble(etanol.getText().toString());
        }

        preferencias.salvarInformacoesPosto(nome.getText().toString(),precoGasCom,precoGasAdd,precoEtan,precoDsel);
        databaseReference = ConfiguracaoFirebase.getFirebase().child("postos_temporarios").child(idPosto);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                databaseReference.setValue(preferencias.getPostoPadrao());
                databaseReferenceAux = ConfiguracaoFirebase.getFirebase().child("usuarios").child(idUsuario).child("cadastrou_posto");
                databaseReferenceAux.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        databaseReferenceAux.setValue(true);
                        Toast.makeText(CadastroInformacoesPostosActivity.this, "Informações enviadas para análise. Se aprovado, o posto será cadastrado.",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(CadastroInformacoesPostosActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
