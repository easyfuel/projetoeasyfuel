package com.ufsj.ocs.projetoocs.helper;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.ufsj.ocs.projetoocs.model.Coordenadas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//Código disponivel em: https://github.com/eddydn/GetCoordinatesGeocode/blob/master/app/src/main/java/dev/edmt/getcoordinatesgeocode/MainActivity.java

public class GeocoderCoordenadas extends AsyncTask<String,Void,String>{
    public GeocoderCoordenadas(ProgressDialog dialogo, Coordenadas coord){
        dialog = dialogo;
        coordenadas = coord;
    }
    private String coordenadasStr;
    private ProgressDialog dialog;
    private Coordenadas coordenadas;

    public Coordenadas getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Coordenadas coordenadas) {
        this.coordenadas = coordenadas;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.setMessage("Buscando endereço...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response;
        try{
            String address = strings[0];
            HttpDataHandler http = new HttpDataHandler();
            String url = String.format("https://maps.googleapis.com/maps/api/geocode/json?address=%s",address);
            response = http.getHTTPData(url);
            return response;
        }
        catch (Exception ex)
        {

        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        try{
            JSONObject jsonObject = new JSONObject(s);

            String lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry")
                    .getJSONObject("location").get("lat").toString();
            String lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry")
                    .getJSONObject("location").get("lng").toString();

            coordenadasStr = String.format("Coordinates : %s / %s ",lat,lng);
            coordenadas.iniciarAsyncCoordenadas(lat,lng);

            if(dialog.isShowing())
                dialog.dismiss();

        } catch (JSONException e) {
            e.printStackTrace();
            coordenadas.iniciarAsyncCoordenadas("0","0");
            if(dialog.isShowing())
                dialog.dismiss();
        }
    }
}


