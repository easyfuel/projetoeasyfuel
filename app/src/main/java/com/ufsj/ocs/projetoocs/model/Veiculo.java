package com.ufsj.ocs.projetoocs.model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.DatabaseReference;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;

/**
 * Created by pedro on 07/11/17.
 */

public class Veiculo{

    private String id;
    private String ano;
    private String marca;
    private String modelo;
    private String motor;
    private String capacidadeTanque;
    private String eficienciaTanque;

    private DatabaseReference databaseReference;

    public Veiculo(){

        this.id = "";
        this.ano = "";
        this.marca = "";
        this.modelo = "";
        this.motor = "";
        this.capacidadeTanque = "";
        this.eficienciaTanque = "";

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getCapacidadeTanque() {
        return capacidadeTanque;
    }

    public void setCapacidadeTanque(String capacidadeTanque) {
        this.capacidadeTanque = capacidadeTanque;
    }

    public String getEficienciaTanque() {
        return eficienciaTanque;
    }

    public void setEficienciaTanque(String eficienciaTanque) {
        this.eficienciaTanque = eficienciaTanque;
    }

    public DatabaseReference editandoVeiculo(String idUser, String veiculoId){
        DatabaseReference alterarVeiculo = ConfiguracaoFirebase.getNodoNomeado("veiculos_usuarios").child("cadastro").child(idUser).child(veiculoId);
        return alterarVeiculo;
    }

    public void salvarDados(String idUsuario, String idVeiculo){

        databaseReference = ConfiguracaoFirebase.getNodoNomeado("veiculos_usuarios");
        databaseReference.child("cadastro").child(idUsuario).child(idVeiculo).setValue(this);

    }

    public String toString(){

        return getMarca() + " " + getModelo() + " " + getAno();
    }
}