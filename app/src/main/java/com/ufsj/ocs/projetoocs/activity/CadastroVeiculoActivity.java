package com.ufsj.ocs.projetoocs.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.Base64Custom;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.Veiculo;
import java.util.ArrayList;
import java.util.Collections;

public class CadastroVeiculoActivity extends AppCompatActivity {

    private Spinner anoSpinner;
    private Spinner marcaSpinner;
    private Spinner modeloSpinner;
    private Spinner motorSpinner;
    private TextView capacidadeTanque;
    private TextView eficienciaTanque;
    private Button cadastrar;
    private DatabaseReference referenciaAno, referenciaMarca,
            referenciaModelo, referenciaMotor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_veiculo);

        anoSpinner = (Spinner) findViewById(R.id.anoId);
        referenciaAno = ConfiguracaoFirebase.getNodoNomeado("veiculos_ano");
        referenciaAno.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                ArrayList<String> lista = new ArrayList<String>();
                int i = 0;
                for (DataSnapshot child : children){
                    lista.add(child.getValue().toString());
                }
                Collections.sort(lista);
                String[] token = new String[lista.size()];
                for (String tok : lista){
                    token[i] = tok;
                    i++;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(CadastroVeiculoActivity.this, android.R.layout.simple_spinner_dropdown_item, token);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                anoSpinner.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        marcaSpinner = (Spinner) findViewById(R.id.marcaId);
        referenciaMarca = ConfiguracaoFirebase.getNodoNomeado("veiculos_marca");
        referenciaMarca.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                ArrayList<String> lista = new ArrayList<String>();
                int i = 0;
                for (DataSnapshot child : children){
                    lista.add(child.getValue().toString());
                }
                Collections.sort(lista);
                String[] token = new String[lista.size()];
                for (String tok : lista){
                    token[i] = tok;
                    i++;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(CadastroVeiculoActivity.this, android.R.layout.simple_spinner_dropdown_item, token);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                marcaSpinner.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        modeloSpinner = (Spinner) findViewById(R.id.modeloId);
        referenciaModelo = ConfiguracaoFirebase.getNodoNomeado("veiculos_modelo");
        referenciaModelo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                ArrayList<String> lista = new ArrayList<String>();
                int i = 0;
                for (DataSnapshot child : children){
                    lista.add(child.getValue().toString());
                }
                Collections.sort(lista);
                String[] token = new String[lista.size()];
                for (String tok : lista){
                    token[i] = tok;
                    i++;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(CadastroVeiculoActivity.this, android.R.layout.simple_spinner_dropdown_item, token);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                modeloSpinner.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        motorSpinner = (Spinner) findViewById(R.id.motorId);
        referenciaMotor = ConfiguracaoFirebase.getNodoNomeado("veiculos_motor");
        referenciaMotor.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                ArrayList<String> lista = new ArrayList<String>();
                int i = 0;
                for (DataSnapshot child : children){
                    lista.add(child.getValue().toString());
                }
                Collections.sort(lista);
                String[] token = new String[lista.size()];
                for (String tok : lista){
                    token[i] = tok;
                    i++;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(CadastroVeiculoActivity.this, android.R.layout.simple_spinner_dropdown_item, token);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                motorSpinner.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        capacidadeTanque = (TextView) findViewById(R.id.capacidadeId);
        eficienciaTanque = (TextView) findViewById(R.id.eficienciaId);

        cadastrar = (Button) findViewById(R.id.botaoCadastrarVeiculoId);
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salvarCadastro();
                Intent intent = new Intent(CadastroVeiculoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }

    public void salvarCadastro(){

        Preferencias preferencias = new Preferencias(CadastroVeiculoActivity.this);
        Veiculo veiculo = new Veiculo();


        String modelo = verificarEntradas(modeloSpinner.getSelectedItem().toString());
        String ano = verificarEntradas(anoSpinner.getSelectedItem().toString());
        String marca = verificarEntradas(marcaSpinner.getSelectedItem().toString());

        String idUser = preferencias.getId();

        String idVeiculo = Base64Custom.codificarBase64(marca + modelo + ano);

        veiculo.setId(idVeiculo);
        veiculo.setAno(ano);
        veiculo.setMarca(marca);
        veiculo.setModelo(modelo);
        veiculo.setMotor(verificarEntradas(motorSpinner.getSelectedItem().toString()));
        veiculo.setCapacidadeTanque(verificarEntradas(capacidadeTanque.getText().toString()));
        veiculo.setEficienciaTanque(verificarEntradas(eficienciaTanque.getText().toString()));

        veiculo.salvarDados(idUser, idVeiculo);

    }

    public String verificarEntradas(String opcao){

        switch (opcao){
            case "- Selecione -":
                return "vazio";
            case "":
                return "vazio";
            default:
                return opcao;
        }

    }

}