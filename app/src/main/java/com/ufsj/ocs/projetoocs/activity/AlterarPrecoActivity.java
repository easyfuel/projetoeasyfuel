package com.ufsj.ocs.projetoocs.activity;

import android.content.DialogInterface;
import android.renderscript.Double2;
import android.renderscript.Float2;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.Coordenadas;
import com.ufsj.ocs.projetoocs.model.Posto;

public class AlterarPrecoActivity extends AppCompatActivity {
    private DatabaseReference databaseReference;
    private TextView nomePosto;
    private EditText gasComum;
    private EditText gasAditivada;
    private EditText etanol;
    private EditText diesel;
    private Button alterar;
    private Posto posto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_preco);
        nomePosto = (TextView) findViewById(R.id.nomePostoAlterarPostos);
        gasComum = (EditText) findViewById(R.id.gasComumAlterarPostosEditText);
        gasAditivada = (EditText) findViewById(R.id.gasAditivadaAlterarPostosEditText);
        etanol = (EditText) findViewById(R.id.etanolAlterarPostosEditText);
        diesel = (EditText) findViewById(R.id.dieselAlterarPostosEditText);
        alterar = (Button) findViewById(R.id.alterarPostosButton);

        Preferencias preferencias = new Preferencias(AlterarPrecoActivity.this);
        posto = preferencias.getPostoPadrao();
        nomePosto.setText(posto.getNome());
        gasComum.setText(Double.toString(posto.getPrecoGasolinaComum()));
        gasAditivada.setText(Double.toString(posto.getPrecoGasolinaAditivada()));
        etanol.setText(Double.toString(posto.getPrecoEtanol()));
        diesel.setText(Double.toString(posto.getPrecoDiesel()));

        alterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog();
            }
        });



    }

    private void alertDialog(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(AlterarPrecoActivity.this);
        alertDialog.setTitle("Confirmar alteração");
        alertDialog.setIcon(R.drawable.ic_directions_car);
        alertDialog.setMessage("Deseja salvar estas informações?");

        alertDialog.setPositiveButton("Alterar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(verificarInformacoes()) {
                    alterarPrecos();
                }
            }
        });

        alertDialog.setNegativeButton("Voltar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertDialog.create();
        alertDialog.show();
    }

    private void alterarPrecos(){

        Float lat = Float.parseFloat(posto.getCoordenadas().getLatitude());
        Float lng = Float.parseFloat(posto.getCoordenadas().getLongitude());
        databaseReference = ConfiguracaoFirebase.getFirebase().child("postos").child(Coordenadas.converterIdCoordenadas(lat,lng));
        databaseReference.setValue(posto);
        Toast.makeText(AlterarPrecoActivity.this,"Preços alterados com sucesso.",Toast.LENGTH_SHORT).show();
        finish();
    }

    private boolean verificarInformacoes(){
        if(!gasComum.getText().toString().isEmpty() && !gasAditivada.getText().toString().isEmpty() && !etanol.getText().toString().isEmpty()
                && !diesel.getText().toString().isEmpty()) {
            if(Double.parseDouble(gasComum.getText().toString())>0.0 && Double.parseDouble(gasAditivada.getText().toString())>0.0 &&
                    Double.parseDouble(etanol.getText().toString())>0.0 && Double.parseDouble(diesel.getText().toString())>0.0) {
                posto.setPrecoGasolinaComum(Double.parseDouble(gasComum.getText().toString()));
                posto.setPrecoGasolinaAditivada(Double.parseDouble(gasAditivada.getText().toString()));
                posto.setPrecoEtanol(Double.parseDouble(etanol.getText().toString()));
                posto.setPrecoDiesel(Double.parseDouble(diesel.getText().toString()));
                return true;
            }else{
                Toast.makeText(AlterarPrecoActivity.this,"Erro: Preço inválido em um dos 4 campos.",Toast.LENGTH_SHORT).show();
            }
        }
        Toast.makeText(AlterarPrecoActivity.this,"Erro: Favor preencher todos os 4 campos.",Toast.LENGTH_SHORT).show();
        return false;
    }
}
