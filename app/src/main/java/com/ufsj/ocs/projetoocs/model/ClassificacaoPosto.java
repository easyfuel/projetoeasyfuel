package com.ufsj.ocs.projetoocs.model;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;

/**
 * Created by pedro on 08/12/17.
 */

public class ClassificacaoPosto {

    String menssagem;
    String usuario;
    String posto;
    float nota;

    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;

    public String getMenssagem() {
        return menssagem;
    }

    public void setMenssagem(String menssagem) {
        this.menssagem = menssagem;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario() {

        firebaseAuth = ConfiguracaoFirebase.getFirebaseAutenticacao();
        this.usuario = firebaseAuth.getCurrentUser().getEmail();
    }

    public String getPosto() {
        return posto;
    }

    public void setPosto(String posto) {
        this.posto = posto;
    }

    public float getNota() {
        return nota;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }

    public void salvarDados(String idPosto, String idUser){
        databaseReference = ConfiguracaoFirebase.getFirebase();
        databaseReference.child("avaliacao_postos").child(idPosto).child(idUser).setValue(this);
    }

    public String toString(){

        return getUsuario() + "\nNota: " + getNota() + "\n" + getMenssagem();
    }
}
