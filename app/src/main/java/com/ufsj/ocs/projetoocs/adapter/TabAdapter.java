package com.ufsj.ocs.projetoocs.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.widget.Switch;

import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.fragment.MapsFragment;
import com.ufsj.ocs.projetoocs.fragment.VeiculosFragment;

/**
 * Created by pedro on 06/11/17.
 */

public class TabAdapter extends FragmentStatePagerAdapter {

    String[] tituloAbas = {"Meus Veículos", "Localizar Postos"};

    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch(position){
            case 0:
                fragment = new VeiculosFragment();
                break;
            case 1:
                fragment = new MapsFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return tituloAbas.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tituloAbas[position];
    }
}
