package com.ufsj.ocs.projetoocs.model;

import com.google.firebase.database.Exclude;

/**
 * Created by JV on 26/10/2017.
 */
public class Posto {

    private String endereco;
    private String nome;
    private Coordenadas coordenadas;
    private double precoGasolinaComum;
    private double precoGasolinaAditivada;
    private double precoEtanol;
    private double precoDiesel;
    private double somaAvaliacoes;
    private double numAvaliacoes;

    public double getSomaAvaliacoes() {
        return somaAvaliacoes;
    }

    public void setSomaAvaliacoes(double somaAvaliacoes) {
        this.somaAvaliacoes = somaAvaliacoes;
    }

    public double getNumAvaliacoes() {
        return numAvaliacoes;
    }

    public void setNumAvaliacoes(double numAvaliacoes) {
        this.numAvaliacoes = numAvaliacoes;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Coordenadas getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Coordenadas coordenadas) {
        this.coordenadas = coordenadas;
    }

    public double getPrecoGasolinaComum() {
        return precoGasolinaComum;
    }

    public void setPrecoGasolinaComum(double precoGasolinaComum) {
        this.precoGasolinaComum = precoGasolinaComum;
    }

    public double getPrecoGasolinaAditivada() {
        return precoGasolinaAditivada;
    }

    public void setPrecoGasolinaAditivada(double precoGasolinaAditivada) {
        this.precoGasolinaAditivada = precoGasolinaAditivada;
    }

    public double getPrecoEtanol() {
        return precoEtanol;
    }

    public void setPrecoEtanol(double precoEtanol) {
        this.precoEtanol = precoEtanol;
    }

    public double getPrecoDiesel() {
        return precoDiesel;
    }

    public void setPrecoDiesel(double precoDiesel) {
        this.precoDiesel = precoDiesel;
    }
}
