package com.ufsj.ocs.projetoocs.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.ClassificacaoPosto;
import com.ufsj.ocs.projetoocs.model.Coordenadas;
import com.ufsj.ocs.projetoocs.model.Posto;
import com.ufsj.ocs.projetoocs.model.Veiculo;

import java.util.ArrayList;

public class AvaliarPostoActivity extends AppCompatActivity {

    private RatingBar classificacao;
    private RatingBar avaliacao;
    private TextView textoMedia;
    private Button botaoAvaliar;
    private ListView listaComentarios;
    private EditText textoAvaliacao;
    private Preferencias preferencias;
    private Posto posto;
    private String idPosto;
    private DatabaseReference referenciaFilho, referenciaPosto;
    private ArrayList<ClassificacaoPosto> avaliacoesusuarios;
    private float soma, total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliar_posto);

        soma = 0;
        total = 0;

        classificacao = (RatingBar) findViewById(R.id.classificaçãoId);

        avaliacao = (RatingBar) findViewById(R.id.ratingAvaliandoId);
        textoMedia = (TextView) findViewById(R.id.textoMediaId);
        botaoAvaliar = (Button) findViewById(R.id.botaoAvaliarId);
        listaComentarios = (ListView) findViewById(R.id.listaComentariosId);
        textoAvaliacao = (EditText) findViewById(R.id.textoDeAvaliacaoId);

        preferencias = new Preferencias(AvaliarPostoActivity.this);
        posto = Preferencias.getPostoPadrao();
        idPosto = Coordenadas.converterIdCoordenadas(Float.parseFloat(posto.getCoordenadas().getLatitude()),Float.parseFloat(posto.getCoordenadas().getLongitude()));

        referenciaFilho = ConfiguracaoFirebase.getNodoNomeado("avaliacao_postos");
        referenciaPosto = referenciaFilho.child(idPosto);

        referenciaPosto.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                avaliacoesusuarios = new ArrayList<>();
                int i = 0;
                for (DataSnapshot child : children){
                    avaliacoesusuarios.add(child.getValue(ClassificacaoPosto.class));
                }
                final String[] lista = new String[avaliacoesusuarios.size()];
                for (ClassificacaoPosto avu : avaliacoesusuarios){
                    lista[i] = avu.toString();
                    soma = soma + avu.getNota();
                    i++;
                }

                total = soma / i;

                classificacao.setRating(total);
                classificacao.setIsIndicator(true);
                if(i != 0)
                    textoMedia.setText("Média: " + total);
                else
                    textoMedia.setText("Posto ainda não avaliado!");

                ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
                        AvaliarPostoActivity.this,
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        lista
                );
                listaComentarios.setAdapter(adaptador);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        botaoAvaliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                avaliar();
                Intent intent = new Intent(AvaliarPostoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }

    private void avaliar(){

        ClassificacaoPosto notaAtribuida = new ClassificacaoPosto();

        Toast.makeText(AvaliarPostoActivity.this, "Foi", Toast.LENGTH_LONG).show();

        notaAtribuida.setMenssagem(textoAvaliacao.getText().toString());
        notaAtribuida.setUsuario();
        notaAtribuida.setPosto(posto.getNome() + " - " + posto.getEndereco());
        notaAtribuida.setNota(avaliacao.getRating());
        notaAtribuida.salvarDados(idPosto , preferencias.getId());

    }
}
