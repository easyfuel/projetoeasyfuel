package com.ufsj.ocs.projetoocs.model;

import android.content.Context;
import android.widget.Toast;

import java.util.Observable;

/**
 * Created by JV on 26/10/2017.
 */

public class Coordenadas extends Observable{
    private String latitude;
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void iniciarAsyncCoordenadas(String lat, String lon){
        synchronized (this){
            this.latitude  = lat;
            this.longitude = lon;
        }
        setChanged();
        notifyObservers();
    }

    public static String converterIdCoordenadas(float lat, float lng){

        String latitude = Float.toString(lat);
        String longitude = Float.toString(lng);
        latitude = latitude.replace(".","D");
        longitude = longitude.replace(".","D");

        if(lat<0){
            latitude = latitude.replace("-","N");
        }else{
            latitude = "P"+latitude;
        }
        //Toast.makeText(context,"teste: "+latitude+longitude,Toast.LENGTH_LONG).show();

        if(lat<0){
            longitude = longitude.replace("-","N");
        }else{
            longitude="P"+longitude;
        }
        return latitude+longitude;
    }

    public static boolean coordenadasIguais(Coordenadas a, Coordenadas b){
        if(Math.abs(Float.parseFloat(a.getLatitude()) -Float.parseFloat(b.getLatitude()))<=0.0002){
            if(Math.abs(Float.parseFloat(a.getLongitude()) -Float.parseFloat(b.getLongitude()))<=0.0002){
                return true;
            }
        }
        return false;
    }
}



