package com.ufsj.ocs.projetoocs.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.activity.AlterarPrecoActivity;
import com.ufsj.ocs.projetoocs.activity.AvaliarPostoActivity;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.DirectionsJSONParser;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.Coordenadas;
import com.ufsj.ocs.projetoocs.model.Posto;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Observable;
import java.util.Observer;


public class MapsFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, LocationListener, Observer {
    @Override
    public void update(Observable observable, Object o) {
        exibirPostos();
    }

    private GoogleMap mMap;
    private LocationManager locationManager;
    private static final String TAG = "MapsFragment";
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceAux;
    private ArrayList<Coordenadas> listaCoordenadas = new ArrayList<Coordenadas>();
    private int numCoord=0;
    private boolean mutexCoordenadas=false;
    private Boolean zoomInicial=false;
    private static Coordenadas coordenadasAtuais = new Coordenadas();
    private boolean rotaTracada=false;
    private Polyline polyline;
    private Context context;
    private Button botao;
    private boolean exibirRota=true;
    private int escolhaCombustivel;
    private HashMap<String,Double> distanciasPostos;
    private int contRotas;
    private ProgressDialog progressDialog;

    private ArrayList<Posto> postosProximos=new ArrayList<Posto>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMapAsync(this);
    }



    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View v = super.onCreateView(layoutInflater,viewGroup,bundle);
        this.context = v.getContext();

        RelativeLayout view = new RelativeLayout(v.getContext());
        view.addView(v, new RelativeLayout.LayoutParams(-1, -1));

        botao = new Button(v.getContext());
        botao.setText("Encontrar Posto");
        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selecionarCombustivel();
            }
        });


        view.addView(botao);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }catch (SecurityException e){

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.removeUpdates(this);
        }catch (SecurityException e){

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            mMap = googleMap;
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.setOnMapClickListener(this);
            mMap.setMyLocationEnabled(true);

        }catch (SecurityException e){
            Log.e(TAG,"Error",e);
        }
        observe(coordenadasAtuais);
    }

    private void adicionarMarcador(LatLng coordenadas, String descricao){
        Coordenadas cord = new Coordenadas();
        cord.setLatitude(Double.toString(coordenadas.latitude));
        cord.setLongitude(Double.toString(coordenadas.longitude));
        MarkerOptions marker = new MarkerOptions();
        marker.position(coordenadas);
        marker.title(descricao);
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.logo_vertical_mini));
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(coordenadas));
        listaCoordenadas.add(cord);

        numCoord++;
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                LatLng latLng =marker.getPosition();
                Coordenadas apiCoord = new Coordenadas();
                apiCoord.setLatitude(Double.toString(latLng.latitude));
                apiCoord.setLongitude(Double.toString(latLng.longitude));
                exibirInformacoesPosto(apiCoord);
                return false;
            }
        });
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    public void observe(Observable o){
        o.addObserver(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if(!zoomInicial) {
            LatLng localizacao = new LatLng(location.getLatitude(), location.getLongitude());
            while(mutexCoordenadas);
            mutexCoordenadas=true;
            coordenadasAtuais.iniciarAsyncCoordenadas(Double.toString(localizacao.latitude),Double.toString(localizacao.longitude));
            mutexCoordenadas=false;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(localizacao,14.0f));//Zoom x14.
            zoomInicial=true;
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Toast.makeText(getContext(),"Status do Provider alterado.",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(getContext(),"Provider habilitado.",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(getContext(),"Provider desabilitado.",Toast.LENGTH_SHORT).show();
    }
    private void exibirPostos(){
        databaseReference = ConfiguracaoFirebase.getFirebase().child("postos");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dados : dataSnapshot.getChildren()) {
                        Posto posto = dados.getValue(Posto.class);

                        Float lat = Float.parseFloat(posto.getCoordenadas().getLatitude());
                        Float lng = Float.parseFloat(posto.getCoordenadas().getLongitude());
                        LatLng destino = new LatLng(Double.parseDouble(lat.toString()),Double.parseDouble(lng.toString()));

                        LatLng origem = recuperarCoordenadaAtual();
                        if(distanciaCoordenadas(origem,destino) <= 20.0) {
                            adicionarMarcador(destino, posto.getNome());
                            postosProximos.add(posto);
                        }
                    }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void exibirInformacoesPosto(final Coordenadas apiCoord){
        databaseReference = ConfiguracaoFirebase.getFirebase().child("postos");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean achou=false;
                String idPosto="";
                for (DataSnapshot dados : dataSnapshot.getChildren()) {
                    Posto posto = dados.getValue(Posto.class);
                    if(Coordenadas.coordenadasIguais(apiCoord,posto.getCoordenadas())){
                        idPosto=Coordenadas.converterIdCoordenadas(Float.parseFloat(posto.getCoordenadas().getLatitude()),Float.parseFloat(posto.getCoordenadas().getLongitude()));
                        achou=true;
                        break;
                    }

                }
                if(achou){
                    databaseReferenceAux = ConfiguracaoFirebase.getFirebase().child("postos").child(idPosto);
                    databaseReferenceAux.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            alertDialogInfoPostos(dataSnapshot.getValue(Posto.class));
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void alertDialogInfoPostos(final Posto posto){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(posto.getNome());
        alertDialog.setIcon(R.drawable.ic_directions_car);
        alertDialog.setMessage("Gasolina Comum: "+posto.getPrecoGasolinaComum()+"\n"+"Gasolina Aditivada: "+posto.getPrecoGasolinaAditivada()
                +"\n"+"Etanol: "+posto.getPrecoEtanol()+"\n"+"Diesel: "+posto.getPrecoDiesel());

        alertDialog.setNeutralButton("Avaliar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Preferencias.setPostoPadrao(posto);
                startActivity(new Intent(getActivity(), AvaliarPostoActivity.class));

            }
        });

        alertDialog.setPositiveButton("Traçar Rota", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                LatLng dest = new LatLng(Double.parseDouble(posto.getCoordenadas().getLatitude()),Double.parseDouble(posto.getCoordenadas().getLongitude()));
                LatLng origem = recuperarCoordenadaAtual();
                tracarRota(origem,dest);
            }
        });

        alertDialog.setNegativeButton("Alterar Preços", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alterarPrecoActivity(posto);
            }
        });

        alertDialog.create();
        alertDialog.show();
    }

    private void alterarPrecoActivity(Posto posto){
        Preferencias preferencias = new Preferencias(getActivity());
        preferencias.setPostoPadrao(posto);
        startActivity(new Intent(getActivity(),AlterarPrecoActivity.class));
    }

    private double grausParaRadianos(double graus) {
        return graus * Math.PI / 180;
    }

    private double distanciaCoordenadas(LatLng o, LatLng d) {
        double raioTerra = 6371;

        double dLat = grausParaRadianos(Math.abs(d.latitude-o.latitude));
        double dLon = grausParaRadianos(Math.abs(d.longitude-o.longitude));

        double lat1 = grausParaRadianos(o.latitude);
        double lat2 = grausParaRadianos(d.latitude);

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return raioTerra * c;
    }

    private void tracarRota(LatLng origin, LatLng dest){
        // Getting URL to the Google Directions API
        limparRotaTracada();
        String url = getDirectionsUrl(origin, dest);
        Double lat = dest.latitude;
        Double lng = dest.longitude;
        DownloadTask downloadTask = new DownloadTask(Coordenadas.converterIdCoordenadas(Float.parseFloat(lat.toString()),
                Float.parseFloat(lng.toString())));

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception download url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String>{
        private String coordenadaId;

        public String getCoordenadaId() {
            return coordenadaId;
        }

        public void setCoordenadaId(String coordenadaId) {
            this.coordenadaId = coordenadaId;
        }

        public DownloadTask(String cord){
            this.setCoordenadaId(cord);
        }

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask(coordenadaId);

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
        private String coordenadaId;

        public String getCoordenadaId() {
            return coordenadaId;
        }

        public void setCoordenadaId(String coordenadaId) {
            this.coordenadaId = coordenadaId;
        }
        public ParserTask(String cord){
            this.setCoordenadaId(cord);
        }


        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if(result.size()<1){
                Toast.makeText(getActivity(), "No Points", Toast.LENGTH_SHORT).show();
                distanciasPostos.put(coordenadaId,0.0);
                contRotas++;
                if(contRotas==postosProximos.size()){
                    calcularMelhorPosto();
                }
                return;
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(3);
                lineOptions.color(Color.RED);
            }
            if(exibirRota) {
                Toast.makeText(getActivity(), "Distance:" + distance + ", Duration:" + duration, Toast.LENGTH_LONG).show();
                System.out.println(distance);
                // Drawing polyline in the Google Map for the i-th route
                polyline = mMap.addPolyline(lineOptions);
                rotaTracada = true;
            }else{
                Double distancia = Double.parseDouble(distance.replace("km",""));
                distanciasPostos.put(coordenadaId,distancia);
                contRotas++;
                if(contRotas==postosProximos.size()){
                    calcularMelhorPosto();
                }
            }
        }
    }

    private void limparRotaTracada(){
        if(rotaTracada){
            polyline.remove();
            rotaTracada=false;
        }

    }

    private void alertDialogEscolherPosto(){
        AlertDialog dialog;
        final String[] combustiveis = {"Gasolina Comum","Gasolina Aditivada","Etanol","Diesel"};
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Escolher combustível");
        escolhaCombustivel=-1;
        alertDialog.setIcon(R.drawable.ic_local_gas_station);

        alertDialog.setSingleChoiceItems(combustiveis, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                escolhaCombustivel=i;
            }
        });

        alertDialog.setPositiveButton("Pesquisar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if(escolhaCombustivel==-1){
                    Toast.makeText(getActivity(),"Favor selecionar uma opção.",Toast.LENGTH_SHORT).show();
                }else{
                    if(postosProximos.size()>0) {
                        recuperarDistanciasPostos();
                    }else{
                        Toast.makeText(getActivity(),"Não existem postos cadastrados por perto.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        alertDialog.setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        dialog = alertDialog.create();
        dialog.show();
    }

    private void selecionarCombustivel(){
        alertDialogEscolherPosto();
    }

    private void iniciarProgressDialog(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Buscando informações dos postos próximos...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }
    private void finalizarProgressDialog(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void recuperarDistanciasPostos(){
        iniciarProgressDialog();
        distanciasPostos = new HashMap<>();
        contRotas=0;
        exibirRota=false;
        for(int i=0;i<postosProximos.size();i++){
            LatLng origem = recuperarCoordenadaAtual();
            LatLng destino = new LatLng(Double.parseDouble(postosProximos.get(i).getCoordenadas().getLatitude())
                    ,Double.parseDouble(postosProximos.get(i).getCoordenadas().getLongitude()));
            tracarRota(origem,destino);
        }
    }

    private void calcularMelhorPosto(){
        Double menorPostoValor=0.0;
        int menorPostoId=0;
        boolean ativou=false;
        for(int i=0;i<postosProximos.size();i++){
            Posto posto = postosProximos.get(i);
            String id = Coordenadas.converterIdCoordenadas(Float.parseFloat(posto.getCoordenadas().getLatitude()),
                    Float.parseFloat(posto.getCoordenadas().getLongitude()));
            Double distancia = distanciasPostos.get(id);

            Double preco = 0.0;
            switch (escolhaCombustivel){
                case 0: preco = posto.getPrecoGasolinaComum();break;
                case 1: preco = posto.getPrecoGasolinaAditivada();break;
                case 2: preco = posto.getPrecoEtanol();break;
                case 3: preco = posto.getPrecoDiesel();
            }
            if(preco == 0.0){
                continue;
            }

            Double multipicadorAvaliacao = 5.0;
            if(posto.getNumAvaliacoes()>0){
                multipicadorAvaliacao = 5 - (posto.getSomaAvaliacoes()/posto.getNumAvaliacoes());
            }

            Double pesoPosto = ((multipicadorAvaliacao/2)+preco)*distancia;
            if(!ativou){
                menorPostoValor=pesoPosto;
                menorPostoId=i;
                ativou=true;
            }
            if(pesoPosto<menorPostoValor){
                menorPostoValor=pesoPosto;
                menorPostoId=i;
            }
            //Log.e("xd","ID: "+id+" Distancia: "+distancia+" Avaliacao: "+multipicadorAvaliacao+" Preco: "+preco+" Peso: "+pesoPosto);
        }
        exibirRota=true;
        LatLng coordenadaMelhorPosto = new LatLng(Double.parseDouble(postosProximos.get(menorPostoId).getCoordenadas().getLatitude()),
                Double.parseDouble(postosProximos.get(menorPostoId).getCoordenadas().getLongitude()));
        tracarRota(recuperarCoordenadaAtual(),coordenadaMelhorPosto);
        finalizarProgressDialog();
        //Log.e("aaa","Melhor Posto: "+postosProximos.get(menorPostoId).getNome()+" Peso: "+menorPostoValor);
        Toast.makeText(getActivity(),"Melhor Posto: "+postosProximos.get(menorPostoId).getNome(),Toast.LENGTH_LONG).show();

    }

    private LatLng recuperarCoordenadaAtual(){
        while(mutexCoordenadas);
        mutexCoordenadas=true;
        LatLng origem = new LatLng(Double.parseDouble(coordenadasAtuais.getLatitude()),Double.parseDouble(coordenadasAtuais.getLongitude()));
        mutexCoordenadas=false;
        return origem;
    }
}
