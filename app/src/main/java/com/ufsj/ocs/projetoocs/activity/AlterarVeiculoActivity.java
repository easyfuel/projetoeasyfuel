package com.ufsj.ocs.projetoocs.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.Veiculo;

import java.util.ArrayList;
import java.util.Collections;

public class AlterarVeiculoActivity extends AppCompatActivity {

    private Spinner anoSpinner;
    private Spinner marcaSpinner;
    private Spinner modeloSpinner;
    private Spinner motorSpinner;
    private TextView capacidadeTanque;
    private TextView eficienciaTanque;
    private Button cadastrar;
    private DatabaseReference referenciaAno, referenciaMarca,
            referenciaModelo, referenciaMotor;


    private DatabaseReference alterarVeiculo, editarVeiculo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_veiculo);

        Preferencias preferencias = new Preferencias(AlterarVeiculoActivity.this);
        String idUser = preferencias.getId();
        Veiculo vehicle = Preferencias.getVeiculoPadrao();
        editarVeiculo = vehicle.editandoVeiculo(idUser, vehicle.getId());


        anoSpinner = (Spinner) findViewById(R.id.anoEditarId);
        referenciaAno = ConfiguracaoFirebase.getNodoNomeado("veiculos_ano");
        referenciaAno.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                ArrayList<String> lista = new ArrayList<String>();
                int i = 0;
                for (DataSnapshot child : children){
                    lista.add(child.getValue().toString());
                }
                Collections.sort(lista);
                String[] token = new String[lista.size()];
                for (String tok : lista){
                    token[i] = tok;
                    i++;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AlterarVeiculoActivity.this, android.R.layout.simple_spinner_dropdown_item, token);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                anoSpinner.setAdapter(adapter);

                if(!vehicle.getAno().equals("vazio")) {
                    int posicaoArray = 0;
                    for (int cont = 0; cont <= token.length; cont++) {

                        if (token[cont].equals(vehicle.getAno())) {

                            posicaoArray = cont;
                            break;
                        }
                    }
                    anoSpinner.setSelection(posicaoArray);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        marcaSpinner = (Spinner) findViewById(R.id.marcaEditarId);
        referenciaMarca = ConfiguracaoFirebase.getNodoNomeado("veiculos_marca");
        referenciaMarca.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                ArrayList<String> lista = new ArrayList<String>();
                int i = 0;
                for (DataSnapshot child : children){
                    lista.add(child.getValue().toString());
                }
                Collections.sort(lista);
                String[] token = new String[lista.size()];
                for (String tok : lista){
                    token[i] = tok;
                    i++;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AlterarVeiculoActivity.this, android.R.layout.simple_spinner_dropdown_item, token);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                marcaSpinner.setAdapter(adapter);

                if(!vehicle.getMarca().equals("vazio")) {
                    int posicaoArray = 0;
                    for (int cont = 0; cont <= token.length; cont++) {

                        if (token[cont].equals(vehicle.getMarca())) {

                            posicaoArray = cont;
                            break;
                        }
                    }
                    marcaSpinner.setSelection(posicaoArray);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        modeloSpinner = (Spinner) findViewById(R.id.modeloEditarId);
        referenciaModelo = ConfiguracaoFirebase.getNodoNomeado("veiculos_modelo");
        referenciaModelo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                ArrayList<String> lista = new ArrayList<String>();
                int i = 0;
                for (DataSnapshot child : children){
                    lista.add(child.getValue().toString());
                }
                Collections.sort(lista);
                String[] token = new String[lista.size()];
                for (String tok : lista){
                    token[i] = tok;
                    i++;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AlterarVeiculoActivity.this, android.R.layout.simple_spinner_dropdown_item, token);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                modeloSpinner.setAdapter(adapter);

                if(!vehicle.getModelo().equals("vazio")) {
                    int posicaoArray = -1;
                    for (int cont = 0; cont <= token.length; cont++) {

                        if (token[cont].equals(vehicle.getModelo())) {

                            posicaoArray = cont;
                            break;
                        }
                    }
                    modeloSpinner.setSelection(posicaoArray);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        motorSpinner = (Spinner) findViewById(R.id.motorEditarId);
        referenciaMotor = ConfiguracaoFirebase.getNodoNomeado("veiculos_motor");
        referenciaMotor.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                ArrayList<String> lista = new ArrayList<String>();
                int i = 0;
                for (DataSnapshot child : children){
                    lista.add(child.getValue().toString());
                }
                Collections.sort(lista);
                String[] token = new String[lista.size()];
                for (String tok : lista){
                    token[i] = tok;
                    i++;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AlterarVeiculoActivity.this, android.R.layout.simple_spinner_dropdown_item, token);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                motorSpinner.setAdapter(adapter);

                if(!vehicle.getMotor().equals("vazio")) {
                    int posicaoArray = -1;
                    for (int cont = 0; cont <= token.length; cont++) {

                        if (token[cont].equals(vehicle.getMotor())) {

                            posicaoArray = cont;
                            break;
                        }
                    }
                    motorSpinner.setSelection(posicaoArray);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        capacidadeTanque = (TextView) findViewById(R.id.textoEditarCapacidadeId);
        if(vehicle.getCapacidadeTanque().equals("vazio"))
            capacidadeTanque.setText("0.0");
        else
            capacidadeTanque.setText(vehicle.getCapacidadeTanque());

        eficienciaTanque = (TextView) findViewById(R.id.textoEditarEficienciaId);
        if(vehicle.getEficienciaTanque().equals("vazio"))
            eficienciaTanque.setText("0.0");
        else
            eficienciaTanque.setText(vehicle.getEficienciaTanque());


        cadastrar = (Button) findViewById(R.id.botaoEditarVeiculoId);
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salvarCadastro(vehicle);
                Intent intent = new Intent(AlterarVeiculoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }

    public void salvarCadastro(Veiculo vehicle){

        Preferencias preferencias = new Preferencias(AlterarVeiculoActivity.this);
        Veiculo veiculo = new Veiculo();

        veiculo.setId(vehicle.getId());
        veiculo.setAno(verificarEntradas(anoSpinner.getSelectedItem().toString()));
        veiculo.setMarca(verificarEntradas(marcaSpinner.getSelectedItem().toString()));
        veiculo.setModelo(verificarEntradas(modeloSpinner.getSelectedItem().toString()));
        veiculo.setMotor(verificarEntradas(motorSpinner.getSelectedItem().toString()));
        veiculo.setCapacidadeTanque(verificarEntradas(capacidadeTanque.getText().toString()));
        veiculo.setEficienciaTanque(verificarEntradas(eficienciaTanque.getText().toString()));

        editarVeiculo.setValue(veiculo);

        Veiculo aux = preferencias.retornarVeiculoSelecionado();
        if(aux.getId().equals(veiculo.getId()))
            preferencias.salvarVeiculoAtual(veiculo);

    }

    public String verificarEntradas(String opcao){

        switch (opcao){
            case "- Selecione -":
                return "vazio";
            case "":
                return "vazio";
            default:
                return opcao;
        }

    }
}
