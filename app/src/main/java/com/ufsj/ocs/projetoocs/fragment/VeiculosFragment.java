package com.ufsj.ocs.projetoocs.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.activity.AlterarVeiculoActivity;
import com.ufsj.ocs.projetoocs.activity.CadastroEnderecoPostosActivity;
import com.ufsj.ocs.projetoocs.activity.CadastroVeiculoActivity;
import com.ufsj.ocs.projetoocs.activity.MainActivity;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.Veiculo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class VeiculosFragment extends Fragment implements OnFailureListener{

    private ListView listaDeCarros;
    private TextView textoVeiculoPrincipal;
    private DatabaseReference referenciaVeiculos, referenciaVeiculosFilho, referenciaFilho, referenciaVeiculoApagar;
    private FirebaseAuth firebaseAuth;
    List<Veiculo> veiculosUser;
    String idUser;
    Preferencias preferencias;
    View view;

    public VeiculosFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        LayoutInflater inflat = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflat.inflate(R.layout.fragment_veiculos, container, false);

        preferencias = new Preferencias(view.getContext());
        idUser = preferencias.getId();
        textoVeiculoPrincipal = (TextView) view.findViewById(R.id.veiculoUsadoID);
        listaDeCarros = (ListView) view.findViewById(R.id.listVeiculosId);

        referenciaVeiculos = ConfiguracaoFirebase.getNodoNomeado("veiculos_usuarios");
        referenciaFilho = referenciaVeiculos.child("cadastro").child(idUser);


        referenciaFilho.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                veiculosUser = new ArrayList<>();
                int i = 0;
                for (DataSnapshot child : children){
                    veiculosUser.add(child.getValue(Veiculo.class));
                }
                final String[] lista = new String[veiculosUser.size()];
                for (Veiculo veic : veiculosUser){
                    lista[i] = veic.toString();
                    i++;
                }

                ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
                        view.getContext(),
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        lista
                );
                listaDeCarros.setAdapter(adaptador);

                if(preferencias.retornarVeiculoSelecionado() == null){
                    textoVeiculoPrincipal.setText("Não há veículos em uso no momento.");
                }
                else{
                    Veiculo veic = preferencias.retornarVeiculoSelecionado();
                    textoVeiculoPrincipal.setText(veic.toString());
                }

                listaDeCarros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        alertDialogVeiculo(veiculosUser.get(position));


                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }



    private void alertDialogVeiculo(Veiculo veiculo){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
        alertDialog.setTitle(veiculo.toString());
        alertDialog.setIcon(R.drawable.ic_action_car);
        alertDialog.setMessage(
                "Ano :  " + veiculo.getAno() + "\n" +
                        "Marca :  " + veiculo.getMarca() + "\n" +
                        "Modelo :  " + veiculo.getModelo() + "\n" +
                        "Motor :  " + veiculo.getMotor() + "\n" +
                        "Tanque(L) :  " + veiculo.getCapacidadeTanque() + "\n" +
                        "Eficiência(KM/L) :  " + veiculo.getEficienciaTanque()
        );
        alertDialog.setPositiveButton("Usar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                preferencias.salvarVeiculoAtual(veiculo);
                textoVeiculoPrincipal.setText(veiculo.toString());

            }
        });

        alertDialog.setNeutralButton("Deletar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {



                referenciaVeiculoApagar = referenciaFilho.child(veiculo.getId());

                Veiculo veic = preferencias.retornarVeiculoSelecionado();

                if(veic.getId().equals(veiculo.getId()))
                    preferencias.salvarVeiculoAtual(null);

                referenciaVeiculoApagar.removeValue();



            }
        });


        alertDialog.setNegativeButton("Editar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                preferencias.setVeiculoPadrao(veiculo);
                startActivity(new Intent(view.getContext(), AlterarVeiculoActivity.class));
            }
        });

        alertDialog.create();
        alertDialog.show();
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
    }
}

    /*private void alertDialogApagarVeiculo(Veiculo veiculo, final int index){
        final Veiculo copiaVeiculo = veiculo;
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EscolherVeiculoActivity.this);
        alertDialog.setTitle("Deseja APAGAR este veículo?");
        alertDialog.setIcon(R.drawable.ic_directions_car);
        alertDialog.setMessage("\nMarca: "+veiculo.getMarca()+"\nModelo: "+veiculo.getModelo()+"\nCapacidade do tanque: "+veiculo.getCapacidadeTanque()+" litros");

        alertDialog.setPositiveButton("APAGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                apagarVeiculo(copiaVeiculo);
                Preferencias preferencias = new Preferencias(EscolherVeiculoActivity.this);
                if(veiculosIguais(copiaVeiculo,preferencias.retornarVeiculoSelecionado())) {
                    //final Preferencias preferencias = new Preferencias(EscolherVeiculoActivity.this);
                    Veiculo vazio = new Veiculo();
                    preferencias.salvarVeiculoAtual(vazio);
                    Toast.makeText(EscolherVeiculoActivity.this, "Veiculo: " + nomesVeiculos.get(index) + " deletado.", Toast.LENGTH_LONG).show();
                }
            }
        });

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertDialog.create();
        alertDialog.show();
    }

    private void apagarVeiculo(final Veiculo copiaVeiculo){
        Preferencias preferencias = new Preferencias(EscolherVeiculoActivity.this);
        final String idUsuarioLogado = preferencias.getId();
        databaseReference = ConfiguracaoFirebase.getFirebase().child("veiculos_usuarios").child(idUsuarioLogado).child("veiculos");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int cont=0;
                boolean removeu=false;
                for(DataSnapshot dados:dataSnapshot.getChildren()){
                    cont++;
                    Veiculo veiculo = dados.getValue(Veiculo.class);
                    if(veiculosIguais(copiaVeiculo,veiculo) && !removeu){
                        removeu = true;
                        idDeletarVeiculo = new String(dados.getKey().toString());
                        databaseReferenceAux = databaseReference.child(idDeletarVeiculo);
                        databaseReferenceAux.removeValue();



                        //Toast.makeText(EscolherVeiculoActivity.this,""+dados.getKey().toString()+"",Toast.LENGTH_LONG).show();
                    }


                }
                if(removeu) {
                    numVeiculos = cont;
                    numVeiculos--;
                    databaseReferenceAux = ConfiguracaoFirebase.getFirebase().child("veiculos_usuarios").child(idUsuarioLogado).child("num_veiculos");
                    databaseReferenceAux.setValue(numVeiculos);
                    Toast.makeText(EscolherVeiculoActivity.this,"numVeiculos: "+numVeiculos+"",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private boolean veiculosIguais(Veiculo original, Veiculo auxiliar){
        return original.getCapacidadeTanque().equals(auxiliar.getCapacidadeTanque()) && original.getModelo().equals(auxiliar.getModelo()) &&
                original.getMarca().equals(auxiliar.getMarca());
    }

}*/
