package com.ufsj.ocs.projetoocs.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.GeocoderCoordenadas;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.Coordenadas;
import com.ufsj.ocs.projetoocs.model.Posto;
import com.ufsj.ocs.projetoocs.model.Veiculo;

import java.util.Observable;
import java.util.Observer;

public class CadastroEnderecoPostosActivity extends AppCompatActivity implements Observer {
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceAux;
    private boolean cadastrouPosto;
    private boolean achou;
    private Button cadastrar;
    private EditText rua;
    private EditText numero;
    private EditText bairro;
    private EditText cidade;
    private EditText estado;
    private Coordenadas coordenadas;
    private String endereco;
    private String idPosto;
    private ProgressDialog progressDialog;
    private Button confirmarPostos;
    private Boolean saiu=false;
    private GeocoderCoordenadas geocoderCoordenadas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cadastro_postos);
        usuarioCadastrouPosto();
        //alertDialogInicial(cadastrouPosto);
        rua = (EditText) findViewById(R.id.enderecoRuaCadastrarPosto);
        confirmarPostos = (Button) findViewById(R.id.confirmarPostosButton);
        numero = (EditText) findViewById(R.id.enderecoNumeroCadastrarPosto);
        cidade = (EditText) findViewById(R.id.enderecoCidadeCadastrarPosto);
        estado = (EditText) findViewById(R.id.enderecoEstadoCadastrarPosto);
        bairro = (EditText) findViewById(R.id.enderecoBairroCadastrarPosto);
        cadastrar = (Button) findViewById(R.id.salvarEnderecoPostoButton);
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!rua.getText().toString().isEmpty() && !numero.getText().toString().isEmpty() && !cidade.getText().toString().isEmpty() &&
                        !estado.getText().toString().isEmpty() && !bairro.getText().toString().isEmpty()){
                    buscarEnderecoGeocode();
                }else{
                    Toast.makeText(CadastroEnderecoPostosActivity.this,"Erro: Favor preencher todos os campos.",Toast.LENGTH_SHORT).show();
                }

            }
        });

        confirmarPostos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CadastroEnderecoPostosActivity.this,ConfirmarPostos.class));
            }
        });

    }

    private void buscarEnderecoGeocode(){
        coordenadas = new Coordenadas();
        observe(coordenadas);
        geocoderCoordenadas = new GeocoderCoordenadas(criarProgressDialog(),coordenadas);
        endereco = new String(numero.getText().toString()+" "+rua.getText().toString()+", "+bairro.getText().toString()+", "+cidade.getText().toString()+", "+
                estado.getText().toString()+", Brazil");
        geocoderCoordenadas.execute(endereco.replace(" ","+"));
    }

    private void alertDialogInicial(boolean flag){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CadastroEnderecoPostosActivity.this);
        alertDialog.setTitle("Cadastrar Posto");
        alertDialog.setIcon(R.drawable.ic_local_gas_station);
        if(!flag) {
            alertDialog.setMessage("Você está prestes a realizar um cadastro de um posto. Essa ação só poderá ser realizada uma vez." +
                    " A veracidade das informações enviadas será averiguada para que este cadastro seja efetivado. Deseja continuar?");
            alertDialog.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        }else{
            alertDialog.setMessage("Você já realizou o cadastro do seu posto.");
        }



        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(CadastroEnderecoPostosActivity.this,MainActivity.class));
                finish();
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.create();
        alertDialog.show();
    }

    private void usuarioCadastrouPosto(){
        Preferencias preferencias = new Preferencias(CadastroEnderecoPostosActivity.this);
        final String idUsuario = preferencias.getId();

        databaseReference = ConfiguracaoFirebase.getFirebase().child("super_usuarios").child(idUsuario);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null){
                    Toast.makeText(CadastroEnderecoPostosActivity.this,"SUPER usuário!",Toast.LENGTH_LONG).show();
                    confirmarPostos.setVisibility(View.VISIBLE);
                    alertDialogInicial(false);

                }else{
                    databaseReferenceAux = ConfiguracaoFirebase.getFirebase().child("usuarios").child(idUsuario).child("cadastrou_posto");
                    databaseReferenceAux.addListenerForSingleValueEvent(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.getValue() == null || dataSnapshot.getValue().toString().equals("false") ){
                                cadastrouPosto=false;
                                alertDialogInicial(false);
                            }else{
                                cadastrouPosto=true;
                                alertDialogInicial(true);


                            }
                            achou=true;
                            //Toast.makeText(CadastroEnderecoPostosActivity.this,"Cadastro do veiculo bem sucedido!",Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public ProgressDialog criarProgressDialog(){
        ProgressDialog dialog = new ProgressDialog(CadastroEnderecoPostosActivity.this);
        return dialog;
    }

    public static void salvarCoordenadas(Coordenadas coord){

    }
    public void observe(Observable o){
        o.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object o) {
        coordenadas = geocoderCoordenadas.getCoordenadas();
        idPosto = Coordenadas.converterIdCoordenadas(Float.parseFloat(coordenadas.getLatitude()),Float.parseFloat(coordenadas.getLongitude()));
        if(!coordenadas.getLatitude().equals("0") && !coordenadas.getLongitude().equals("0")) {
            //Toast.makeText(CadastroEnderecoPostosActivity.this, "Coordenadas: " + coordenadas.getLatitude() + " " + coordenadas.getLongitude() + "", Toast.LENGTH_LONG).show();
            Preferencias preferencias = new Preferencias(CadastroEnderecoPostosActivity.this);
            preferencias.salvarEnderecoPosto(endereco,coordenadas);
            progressDialog = new ProgressDialog(CadastroEnderecoPostosActivity.this);
            progressDialog.setMessage("Verificando o banco de dados...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            databaseReference = ConfiguracaoFirebase.getFirebase().child("postos_temporarios");
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    achou = false;
                    for(DataSnapshot dados:dataSnapshot.getChildren()){
                        //Posto posto = dados.getValue(Posto.class);
                        if(idPosto.equals(dados.getKey())){
                            achou=true;
                            break;
                        }
                    }
                    if(achou && !saiu){
                        fecharDialog(progressDialog);
                        Toast.makeText(CadastroEnderecoPostosActivity.this, "Erro: Esse posto já existe.", Toast.LENGTH_LONG).show();
                    }else{
                        databaseReferenceAux = ConfiguracaoFirebase.getFirebase().child("postos");
                        databaseReferenceAux.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(!achou) {
                                    for (DataSnapshot dados : dataSnapshot.getChildren()) {
                                        //Posto posto = dados.getValue(Posto.class);
                                        if (idPosto.equals(dados.getKey())) {
                                            achou = true;
                                            break;
                                        }
                                    }
                                    fecharDialog(progressDialog);
                                    if (achou && !saiu) {
                                        Toast.makeText(CadastroEnderecoPostosActivity.this, "Erro: Esse posto já existe.", Toast.LENGTH_LONG).show();
                                    } else {
                                        finish();
                                        finish();
                                        startActivity(new Intent(CadastroEnderecoPostosActivity.this, CadastroInformacoesPostosActivity.class));
                                        saiu=true;
                                    }
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });



        }else{
            Toast.makeText(CadastroEnderecoPostosActivity.this, "Erro: Endereço não encontrado.", Toast.LENGTH_LONG).show();
        }
    }
    private void fecharDialog(ProgressDialog dialog){
        if(dialog.isShowing())
            dialog.dismiss();
    }
}
