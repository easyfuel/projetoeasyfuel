package com.ufsj.ocs.projetoocs.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.ufsj.ocs.projetoocs.model.Coordenadas;
import com.ufsj.ocs.projetoocs.model.Posto;
import com.ufsj.ocs.projetoocs.model.Veiculo;

import java.util.HashMap;

public class Preferencias {

    private Context contexto;
    private SharedPreferences preferences;
    private final String NOME_ARQUIVO = "ufsj.preferencias";
    private final int MODE = 0;
    private SharedPreferences.Editor editor;

    private final String CHAVE_ID = "idUsuarioLogado";
    private final String NUM_VEICULOS = "0";
    private final String EXISTE_VEICULO = "0";
    private final String VEICULO_ATUAL_ID = "idVeiculo";
    private final String VEICULO_ATUAL_MARCA = "marcaVeiculo";
    private final String VEICULO_ATUAL_MODELO = "modeloVeiculo";
    private final String VEICULO_ATUAL_MOTOR = "motorVeiculo";
    private final String VEICULO_ATUAL_ANO = "anoVeiculo";
    private final String VEICULO_ATUAL_CAPACIDADE_TANQUE = "capacidadeVeiculo";
    private final String VEICULO_ATUAL_EFICIENCIA_TANQUE = "eficienciaVeiculo";

    private static Posto posto;
    private static Veiculo veiculo;

    public static Posto getPostoPadrao() {
        return posto;
    }

    public static void setPostoPadrao(Posto posto) {
        Preferencias.posto = posto;
    }

    public static Veiculo getVeiculoPadrao() {
        return veiculo;
    }

    public static void setVeiculoPadrao(Veiculo veiculo) {
        Preferencias.veiculo = veiculo;
    }

    public Preferencias(Context contextoParametro){

        contexto = contextoParametro;
        preferences = contexto.getSharedPreferences(NOME_ARQUIVO, MODE );
        editor = preferences.edit();

    }

    public void salvarDados(String idUsuario){

        editor.putString(CHAVE_ID,idUsuario );
        editor.commit();
    }

    public void salvarEnderecoPosto(String endereco, Coordenadas coordenadas){
        posto = new Posto();
        posto.setEndereco(endereco);
        posto.setCoordenadas(coordenadas);

    }

    public void salvarInformacoesPosto(String nome, double pGasolinaComum, double pGasolinaAditivada, double pEtanol, double pDiesel){
        if(posto == null){
            posto = new Posto();
        }
        posto.setNome(nome);
        posto.setPrecoGasolinaComum(pGasolinaComum);
        posto.setPrecoGasolinaAditivada(pGasolinaAditivada);
        posto.setPrecoEtanol(pEtanol);
        posto.setPrecoDiesel(pDiesel);
        posto.setNumAvaliacoes(0.0);
        posto.setSomaAvaliacoes(0.0);
    }

    public void salvarNumeroVeiculos(String numVeiculos){
        editor.putString(NUM_VEICULOS,numVeiculos);
        editor.commit();
    }

    public void salvarVeiculoAtual(Veiculo veiculo){

        if(veiculo == null) {
            editor.remove(EXISTE_VEICULO);
            editor.remove(VEICULO_ATUAL_ID);
            editor.remove(VEICULO_ATUAL_MARCA);
            editor.remove(VEICULO_ATUAL_ANO);
            editor.remove(VEICULO_ATUAL_MODELO);
            editor.remove(VEICULO_ATUAL_MOTOR);
            editor.remove(VEICULO_ATUAL_EFICIENCIA_TANQUE);
            editor.remove(VEICULO_ATUAL_CAPACIDADE_TANQUE);
            editor.commit();
        }
        else {
            editor.putString(EXISTE_VEICULO, "1");
            editor.putString(VEICULO_ATUAL_ID, veiculo.getId());
            editor.putString(VEICULO_ATUAL_MARCA, veiculo.getMarca());
            editor.putString(VEICULO_ATUAL_ANO, veiculo.getAno());
            editor.putString(VEICULO_ATUAL_MODELO, veiculo.getModelo());
            editor.putString(VEICULO_ATUAL_MOTOR, veiculo.getMotor());
            editor.putString(VEICULO_ATUAL_EFICIENCIA_TANQUE, veiculo.getEficienciaTanque());
            editor.putString(VEICULO_ATUAL_CAPACIDADE_TANQUE, veiculo.getCapacidadeTanque());
            editor.commit();
        }
    }

    public String getNumeroVeiculos(){
        return preferences.getString(NUM_VEICULOS,null);
    }

    public String getId(){
        return preferences.getString(CHAVE_ID,null);
    }

    public Veiculo retornarVeiculoSelecionado(){

        if(preferences.contains(EXISTE_VEICULO)) {


            Veiculo veiculo = new Veiculo();

            veiculo.setId(preferences.getString(VEICULO_ATUAL_ID, null));
            veiculo.setAno(preferences.getString(VEICULO_ATUAL_ANO, null));
            veiculo.setMotor(preferences.getString(VEICULO_ATUAL_MOTOR, null));
            veiculo.setEficienciaTanque(preferences.getString(VEICULO_ATUAL_EFICIENCIA_TANQUE, null));
            veiculo.setMarca(preferences.getString(VEICULO_ATUAL_MARCA, null));
            veiculo.setModelo(preferences.getString(VEICULO_ATUAL_MODELO, null));
            veiculo.setCapacidadeTanque(preferences.getString(VEICULO_ATUAL_CAPACIDADE_TANQUE, null));

            return veiculo;

        }
        else
            return null;

    }

}
