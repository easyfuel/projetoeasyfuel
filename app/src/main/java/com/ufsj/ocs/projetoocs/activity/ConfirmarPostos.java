package com.ufsj.ocs.projetoocs.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.model.Coordenadas;
import com.ufsj.ocs.projetoocs.model.Posto;
import com.ufsj.ocs.projetoocs.model.Veiculo;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ConfirmarPostos extends AppCompatActivity {
    private ListView listView;
    private ArrayList<String> idPostos;
    private ArrayList<String> nomesPostos;
    private ArrayList<Posto> postos;
    private ArrayAdapter arrayAdapter;
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceAux;
    private Preferencias preferencias;
    private int indice;
    private String idPosto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_postos);
        exibirPostosTemporarios();
    }

    private void exibirPostosTemporarios(){
        listView = (ListView) findViewById(R.id.confirmarPostosListView);
        nomesPostos = new ArrayList<String>();
        postos = new ArrayList<Posto>();
        idPostos = new ArrayList<String>();
        arrayAdapter = new ArrayAdapter(ConfirmarPostos.this,R.layout.lista_contatos,nomesPostos);

        preferencias = new Preferencias(ConfirmarPostos.this);
        databaseReference = ConfiguracaoFirebase.getFirebase().child("postos_temporarios");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                idPostos.clear();
                nomesPostos.clear();
                postos.clear();
                int i=0;
                for(DataSnapshot dados:dataSnapshot.getChildren()){
                    i++;
                    Posto posto = dados.getValue(Posto.class);
                    idPostos.add(dados.getKey());
                    postos.add(posto);
                    nomesPostos.add(posto.getNome());

                }
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                indice = i;
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ConfirmarPostos.this);
                alertDialog.setTitle("Cadastrar posto?");

                alertDialog.setMessage(postos.get(indice).getNome()+"\n"+postos.get(indice).getEndereco());
                alertDialog.setPositiveButton("CADASTRAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        cadastrarPosto(idPostos.get(indice),indice);
                        apagarPostoTemporario(idPostos.get(indice),indice,false);
                    }
                });

                alertDialog.setNegativeButton("APAGAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        apagarPostoTemporario(idPostos.get(indice),indice,true);
                    }
                });
                alertDialog.setCancelable(true);
                alertDialog.create();
                alertDialog.show();
            }
        });

    }
    private void cadastrarPosto(String idPosto, int i){
        databaseReference = ConfiguracaoFirebase.getFirebase().child("postos").child(idPosto);
        databaseReference.setValue(postos.get(i));
        Toast.makeText(ConfirmarPostos.this, "Posto disponível para os usuários.", Toast.LENGTH_LONG).show();
    }
    private void apagarPostoTemporario(String id, int i, boolean flag){
        //Toast.makeText(ConfirmarPostos.this, "id:"+id, Toast.LENGTH_LONG).show();
        databaseReferenceAux = ConfiguracaoFirebase.getFirebase().child("postos_temporarios").child(id);
        databaseReferenceAux.removeValue();

        postos.remove(i);
        idPostos.remove(i);
        nomesPostos.remove(i);
        arrayAdapter.notifyDataSetChanged();
        if(flag) {
            Toast.makeText(ConfirmarPostos.this, "Posto apagado com sucesso.", Toast.LENGTH_LONG).show();
        }
    }
}
