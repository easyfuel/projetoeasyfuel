package com.ufsj.ocs.projetoocs.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.ufsj.ocs.projetoocs.R;
import com.ufsj.ocs.projetoocs.adapter.TabAdapter;
import com.ufsj.ocs.projetoocs.config.ConfiguracaoFirebase;
import com.ufsj.ocs.projetoocs.fragment.MapsFragment;
import com.ufsj.ocs.projetoocs.helper.Base64Custom;
import com.ufsj.ocs.projetoocs.helper.Preferencias;
import com.ufsj.ocs.projetoocs.helper.SlidingTabLayout;

public class MainActivity extends AppCompatActivity{
    private FirebaseAuth firebaseAuth;
    private FragmentManager fragmentManager;
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceAux;
    private Toolbar toolbar;
    private Integer numVeiculos;
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    //private Preferencias preferencias;
    private String[] permissoesNecessarias = new String[]{
            android.Manifest.permission.INTERNET, android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //criarSuperUsuario();
        /*
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("EasyFuel");
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.containerMain,new MapsFragment(),"MapsFragment");
        transaction.commitAllowingStateLoss();
        verificarVeiculoCadastrado();
        */
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Easy Fuel");
        setSupportActionBar(toolbar);

        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.stl_tabs);
        viewPager = (ViewPager) findViewById(R.id.pagina_view);

        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this, R.color.colorYellow));

        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabAdapter);

        slidingTabLayout.setViewPager(viewPager);
    }

    private void showFragment(Fragment fragment, String name ){
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.containerMain,fragment,name);
        transaction.commit();
    }
    /*
    @Override
    public boolean onNavigationItemSelected(MenuItem item){
        int id = item.getItemId();
        switch(id){
            case R.id.nav_exemploproviderv1:
                    showFragment(new MapsFragment(),"MapsFragment");
                break;
        }

    }
    */


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_main_drawer,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_logout:
                logoutUsuario();
                return true;
            case R.id.cadastroVeicularId:
                startActivity(new Intent(MainActivity.this,CadastroVeiculoActivity.class));
                finish();
                return true;
            case R.id.item_criar_posto:
                startActivity(new Intent(MainActivity.this, CadastroEnderecoPostosActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public int verificarVeiculoCadastrado(){
        final Preferencias preferencias = new Preferencias(MainActivity.this);
        String identificadorContato = preferencias.getId();
        //Toast.makeText(MainActivity.this,"iduser: "+identificadorContato+"",Toast.LENGTH_LONG).show();


        databaseReference = ConfiguracaoFirebase.getFirebase().child("veiculos_usuarios").child(identificadorContato).child("num_veiculos");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(Integer.class) == null){
                    numVeiculos=0;
                    preferencias.salvarNumeroVeiculos(numVeiculos.toString());
                    databaseReference.setValue(numVeiculos);

                }else {
                    numVeiculos = dataSnapshot.getValue(Integer.class);
                }
                //Toast.makeText(MainActivity.this," Existem "+numVeiculos+" veiculos!",Toast.LENGTH_LONG).show();
                if(numVeiculos == 0){

                    criarAlertDialogVeiculo();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*
        if(numVeiculos==0){
            //final Integer numVeiculos = 0;
            preferencias.salvarNumeroVeiculos("0");
            databaseReference = ConfiguracaoFirebase.getFirebase().child("veiculos_usuarios").child(identificadorContato);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Preferencias preferencias = new Preferencias(MainActivity.this);
                    String identificadorContato = preferencias.getId();
                    databaseReference = ConfiguracaoFirebase.getFirebase().child("veiculos_usuarios").child(identificadorContato).child("num_veiculos");
                    databaseReference.setValue(numVeiculos);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            databaseReference.setValue(numVeiculos);
            criarAlertDialogVeiculo();
            return 0;
        }
        preferencias.salvarNumeroVeiculos(numVeiculos.toString());
        return numVeiculos;
        */
        return 0;


    }

    public void criarAlertDialogVeiculo(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle("Cadastrar Veículo");
        alertDialog.setIcon(R.drawable.ic_directions_car);
        alertDialog.setMessage("Você ainda não possui nenhum veículo. Deseja cadastrar um veículo agora?");

        alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(MainActivity.this,CadastroVeiculoActivity.class));
            }
        });

        alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this,"Você pode cadastrar um veículo mais tarde na opção: 'Cadastrar Veículo'.",Toast.LENGTH_LONG).show();
            }
        });

        alertDialog.create();
        alertDialog.show();
    }

    public void logoutUsuario(){
        firebaseAuth = ConfiguracaoFirebase.getFirebaseAutenticacao();
        firebaseAuth.signOut();
        startActivity(new Intent(MainActivity.this,LoginActivity.class));
        finish();
    }
    /*
    private void criarSuperUsuario(){
        Preferencias preferencias = new Preferencias(MainActivity.this);
        String idUsuario;
        String[] nomes = {"negopedron@gmail.com","italo.ufsj@gmail.com","jvgoncalves935@hotmail.com"};
        for(int i=0;i<3;i++) {
            final DatabaseReference databaseReferenceAux = ConfiguracaoFirebase.getFirebase().child("super_usuarios").child(Base64Custom.codificarBase64(nomes[i])).child("cadastrou_posto");
            databaseReferenceAux.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    databaseReferenceAux.setValue(true);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }*/



}
